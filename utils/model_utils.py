# !/usr/bin/env python3
# -*- coding:utf8 -*-
import sys
try:
    reload(sys)
    sys.setdefaultencoding('utf8')
except:
    pass

import re, os
import glob
import const
import torch
import numpy as np

def save_model(model, epoch, score, save_dir=const.model_dir, max_keep=2):
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    f_list = glob.glob(os.path.join(save_dir, 'model') + '_*.ckpt')
    if len(f_list) >= max_keep + 2:
        epoch_list = [int(i.split('_')[-2].split('.')[0]) for i in f_list]
        to_delete = [f_list[i] for i in np.argsort(epoch_list)[-max_keep:]]
        for f in to_delete:
            os.remove(f)
    name = 'model_%d_%.3f.ckpt' % (epoch, score)
    file_path = os.path.join(save_dir, name)
    #torch.save(model.state_dict(), file_path)
    torch.save(model, file_path)

def load_model(model, save_dir=const.model_dir):
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    f_list = glob.glob(os.path.join(save_dir, 'model') + '_*.ckpt')
    start_epoch = 1
    score = 0.0
    if len(f_list) >= 1:
        epoch_list = [int(i.split('_')[-2].split('.')[0]) for i in f_list]
        score_list = [float('.'.join(i.split('_')[-1].split('.')[:-1])) for i in f_list]
        last_checkpoint = f_list[np.argmax(epoch_list)]
        if os.path.exists(last_checkpoint):
            print('load from {}'.format(last_checkpoint))
            #model.load_state_dict(torch.load(last_checkpoint))
            model = torch.load(last_checkpoint)
            seq_pairs = sorted(zip(epoch_list, score_list), key=lambda p: p[0], reverse=True)
            epoch_list, score_list = zip(*seq_pairs)
            start_epoch, score = epoch_list[0], score_list[0]
    return model, start_epoch, score

def load_evaluate_model(model):
    model.load_state_dict(torch.load(const.model_path))
    return model