# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
try:
	reload(sys)
	sys.setdefaultencoding('utf8')
except:
	pass

vocab_dir = './data/vocab/'
model_dir = './data/models/'
result_dir = './data/results/'
model_dir = './data/models/'

train_path = ['./data/preprocessed/trainset/search.train.json']
dev_path = ['./data/preprocessed/devset/search.dev.json']
test_path = ['./data/preprocessed/testset/search.test.json']
predict_path= ['./data/test1set/preprocessed/search.test1.json', './data/test1set/preprocessed/zhidao.test1.json']
log_path = './log.txt'

question_types = []

stopword_path = './data/stopwords.sign.txt'

# tokens
pad = '<pad>'
unk = '<unk>'

pad_idx = 0
unk_idx = 1

max_p_num = 5
max_p_len = 500
max_q_len = 60
max_a_len = 200

# neural network hparameters
lr_rate = 1e-6
batch_size = 4
embed_size = 120
hidden_size = 128
num_layers = 1
grad_clip = 5.0
dropout_p = 0.5

epochs = 200
print_every = 50

import torch
use_cuda = torch.cuda.is_available()
