# !/usr/bin/env python
# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

class StackRNN(nn.Module):
    def __init__(self, input_size, hidden_size, rnn_type=nn.GRU, num_layers=1, dropout_p=0.2, use_pad=True):
        super(StackRNN, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.dropout_p = dropout_p
        self.use_pad = use_pad
        self.rnns = nn.ModuleList()
        for i in range(self.num_layers):
            input_size = input_size if i == 0 else 2 * hidden_size
            self.rnns.append(rnn_type(input_size, hidden_size, bidirectional=True, dropout=self.dropout_p))

    def set_use_pad(flag):
        self.use_pad = flag

    def get_pad_input(self, x, x_mask, lengths=None):
        #lengths = torch.LongTensor(lengths)
        lengths = x_mask.data.eq(1.0).long().sum(1).squeeze()
        exit()
        _, idx_sort = torch.sort(lengths, dim=0, descending=True)
        _, idx_unsort = torch.sort(idx_sort, dim=0)
        lengths = list(lengths[idx_sort])
        idx_sort = Variable(idx_sort)
        idx_unsort = Variable(idx_unsort)
        # Sort x
        x = x.index_select(0, idx_sort)
        x = x.cuda() if torch.cuda.is_available() else x
        x = x.transpose(0, 1).contiguous()

        pack_input = pack_padded_sequence(x, lengths)

        return pack_input, idx_unsort

    def get_pad_output(self, outputs, x_mask, idx_unsort):
        for i, o in enumerate(outputs[1:], 1):
            outputs[i] = pad_packed_sequence(o)[0]

        output = torch.cat(outputs[1:], 2)
        # output = outputs[-1]
        output = output.transpose(0, 1)
        output = output.index_select(0, idx_unsort)

        if output.size(1) != x_mask.size(1):
            padding = torch.zeros(output.size(0), x_mask.size(1) - output.size(1), output.size(2)).type(output.data.type())
            output = torch.cat([output, Variable(padding)], 1)
        return output

    def forward(self, x, x_mask=None, x_len=None):
        if self.use_pad:
            x = x.transpose(0, 1)
            outputs = [x]
            for i in range(self.num_layers):
                rnn_input = outputs[-1]
                rnn_output = self.rnns[i](rnn_input)[0]
                outputs.append(rnn_output)
            output = torch.cat(outputs[1:], 2)
            #output = outputs[-1]
            output = output.transpose(0, 1)
        else:
            rnn_input, idx_unsort = self.get_pad_input(x, x_mask, x_len)
            outputs = [rnn_input]
            for i in range(self.num_layers):
                rnn_input = outputs[-1]
                outputs.append(self.rnns[i](rnn_input)[0])

            output = self.get_pad_output(outputs, x_mask, idx_unsort)
        return output

class BiRNN(nn.Module):

    def __init__(self, input_size, hidden_size, rnn_type=nn.GRU, num_layers=1, dropout_p=0.2):
        super(BiRNN, self).__init__()

        self.rnn_type = rnn_type
        self.hidden_size = hidden_size
        self.rnn = rnn_type(input_size=input_size, hidden_size=hidden_size,
                           num_layers=num_layers,
                           dropout=dropout_p,
                           bidirectional=True,
                           batch_first=True)


    def forward(self, inputs):
        batch_size, seq_len, feature_size = inputs.size()
        h_0 = Variable(torch.zeros(2, batch_size, self.hidden_size), requires_grad=False)
        h_0 = h_0.cuda() if torch.cuda.is_available() else h_0
        if isinstance(self.rnn_type, nn.LSTM):
            c_0 = Variable(torch.zeros(2, batch_size, self.hidden_size), requires_grad=False)
            c_0 = c_0.cuda() if torch.cuda.is_available() else c_0
            outputs, (h_n, c_n) = self.rnn(inputs, (h_0, c_0)) 
        else:
            outputs, _ = self.rnn(inputs, h_0) 
        return outputs
