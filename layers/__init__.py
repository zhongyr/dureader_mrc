# !/usr/bin/env python
# -*- coding: utf-8 -*-

import torch
from torch.autograd import Variable
import torch.nn.functional as F

use_cuda = torch.cuda.is_available()

def weighted_sum(x, attn):
    if attn.dim() == 2 and x.dim() == 3:
        return attn.unsqueeze(1).bmm(x).squeeze(1)
    if attn.dim() == 3 and x.dim() == 3:
        return attn.bmm(x)
    if x.dim() - 1 < attn.dim():
        expanded_size = list(x.size())
        for i in range(attn.dim() - x.dim() + 1):
            x = x.unsqueeze(1)
            expanded_size.insert(i + 1, attn.size(i + 1))
        x = x.expand(*expanded_size)
    intermediate = attn.unsqueeze(-1).expand_as(x) * x
    return intermediate.sum(dim=-2)

def masked_softmax(x, mask=None):
    if mask is None:
        result = F.softmax(x, dim=-1)
    else:
        result = F.softmax(x * mask, dim=-1)
        result = result * mask
        result = result / (result.sum(dim=1, keepdim=True) + 1e-13)
    return result

def last_dim_softmax(x, mask):
    x_shape = x.size()
    reshaped_x = x.view(-1, x.size()[-1])
    if mask is not None:
        while mask.dim() < x.dim():
            mask = mask.unsqueeze(1)
        mask = mask.expand_as(x).contiguous().float()
        mask = mask.view(-1, mask.size()[-1])
    reshaped_result = masked_softmax(reshaped_x, mask)
    return reshaped_result.view(*x_shape)

def replace_masked_values(x, mask, replace_with=-1e-7):
    if x.dim() != mask.dim():
        raise ValueError("x.dim() (%d) != mask.dim() (%d)" % (x.dim(), mask.dim()))
    one_minus_mask = 1.0 - mask
    values_to_add = replace_with * one_minus_mask
    return x * mask + values_to_add

def get_mask(x, lengths):
    if isinstance(x, Variable):
        x = x.data
    '''
    max_len = x.size(1)
    idxes = torch.arange(0, max_len, out=torch.LongTensor(max_len)).unsqueeze(0)
    idxes = idxes.cuda() if use_cuda else idxes
    mask = Variable((idxes < lengths.unsqueeze(1)).float())
    #print(mask)
    #exit()
    '''
    mask = torch.ne(x, 0).float()
    mask = mask.cuda() if use_cuda else mask
    mask = Variable(mask)
    return mask
