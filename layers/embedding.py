# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import torch
import torch.nn as nn

class WordEmbedding(nn.Module):
    '''
    In : (N, sentence_len)
    Out: (N, sentence_len, embd_size)
    '''
    def __init__(self, input_size, embed_size, padding_idx = 0, pre_embd_w=None):
        super(WordEmbedding, self).__init__()
        self.embedding = nn.Embedding(input_size, embed_size, padding_idx=padding_idx)
        if pre_embd_w is not None:
            self.embedding.weight = nn.Parameter(pre_embed_w, requires_grad=False)

    def forward(self, x):
        return self.embedding(x)

class CNNEmbedding(nn.Module):
    def __init__(self, input_size, embed_size, batch_size, kernel_size=5, padding_idx=0):
        super(CNNEmbedding, self).__init__()
        self.embedding = nn.Embedding(input_size, embed_size, padding_idx=padding_idx)
        self.conv2d = nn.Conv2d(1, embed_size, kernel_size, padding=(kernel_size-1)//2)

    def forward(self, x):
        embed = self.embedding(x).unsqueeze(1)
        embed = self.conv2d(embed)
        embed = torch.max(embed, 1)[0]
        return embed
