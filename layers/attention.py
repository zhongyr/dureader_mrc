# !/usr/bin/env python
# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn.parameter import Parameter


class BahdanauAttention(nn.Module):
    def __init__(self, num_units, query_size, memory_size):
        super(BahdanauAttention, self).__init__()

        self._num_units = num_units
        self._softmax = nn.Softmax()

        self.query_layer = nn.Linear(query_size, num_units, bias=False)
        self.memory_layer = nn.Linear(memory_size, num_units, bias=False)
        self.alignment_layer = nn.Linear(num_units, 1, bias=False)

    def _score(self, query, keys):
        # Put the query and the keys into Dense layer
        processed_query = self.query_layer(query)
        values = self.memory_layer(keys)
        extended_query = processed_query.unsqueeze(1)
        alignment = self.alignment_layer(F.tanh(extended_query + values))

        return alignment.squeeze(2)

    def forward(self, query, keys):
        # Calculate the alignment score
        alignment_score = self._score(query, keys)

        # Put it into softmax to get the weight of every steps
        weight = F.softmax(alignment_score, dim=-1)

        # To get the context, this is the original formula
        # context = sum(weight * keys)
        # In order to multiply those two, we need to reshape the weight
        # from [B x S] into [B x S x 1] for broacasting.
        # The multiplication will result in [B x S x embedding]. Remember,
        # we want the score as the sum over all the steps. Therefore, we will
        # sum it over the 1st index
        context = weight.unsqueeze(2) * keys
        total_context = context.sum(1)

        return total_context, alignment_score


class LuongAttention(nn.Module):
    _SCORE_FN = {
        "dot": "_dot_score",
        "general": "_general_score",
        "concat": "_concat_score"
    }

    def __init__(self,
                 attention_window_size,
                 num_units,
                 query_size,
                 memory_size,
                 alignment="local",
                 score_fn="dot"):
        super(LuongAttention, self).__init__()

        if score_fn not in self._SCORE_FN.keys():
            raise ValueError()

        self._attention_window_size = attention_window_size
        self._softmax = nn.Softmax()
        self._score_fn = score_fn
        self._alignment = alignment

        self.query_layer = nn.Linear(query_size, num_units, bias=False)
        self.predictive_alignment_layer = nn.Linear(num_units, 1, bias=False)
        self.alignment_layer = nn.Linear(num_units, 1, bias=False)

        if score_fn == "general":
            self.general_memory_layer = nn.Linear(
                memory_size, query_size, bias=False)
        elif score_fn == "concat":
            self.concat_memory_layer1 = nn.Linear(
                2 * memory_size, num_units, bias=False)
            self.concat_memory_layer2 = nn.Linear(num_units, 1, bias=False)

    def _dot_score(self, query, keys):
        depth = query.size(-1)
        key_units = keys.size(-1)
        if depth != key_units:
            raise ValueError(
                "Incompatible inner dimensions between query and keys. "
                "Query has units: %d. Keys have units: %d. "
                "Dot score requires you to have same size between num_units in "
                "query and keys" % (depth, key_units))

        # Expand query to [B x 1 x embedding dim] for broadcasting
        extended_query = query.unsqueeze(1)

        # Transpose the keys so that we can multiply it
        tkeys = keys.transpose(1, 2)

        alignment = torch.matmul(extended_query, tkeys)

        # Result of the multiplication will be in size [B x 1 x embedding dim]
        # we can safely squeeze the dimension
        return alignment.squeeze(1)

    def _general_score(self, query, keys):
        weighted_keys = self.general_memory_layer(keys)
        extended_query = query.unsqueeze(1)
        weighted_keys = weighted_keys.transpose(1, 2)

        alignment = torch.matmul(extended_query, weighted_keys)
        return alignment.squeeze(1)

    def _concat_score(self, query, keys):
        expanded_query = query.unsqueeze(1).expand(*keys.size())
        concatenated_hidden = torch.cat([expanded_query, keys], dim=2)
        weighted_concatenated_hidden = self.concat_memory_layer1(
            concatenated_hidden)
        temp_score = F.tanh(weighted_concatenated_hidden)
        alignment = self.concat_memory_layer2(temp_score)

        return alignment.squeeze(2)

    def forward(self, query, keys, key_lengths):
        score_fn = getattr(self, self._SCORE_FN[self._score_fn])
        alignment_score = score_fn(query, keys)

        weight = F.softmax(alignment_score, dim=-1)

        if self._alignment == "local":
            extended_key_lengths = key_lengths.unsqueeze(1)
            preprocessed_query = self.query_layer(query)

            activated_query = F.tanh(preprocessed_query)
            sigmoid_query = F.sigmoid(
                self.predictive_alignment_layer(activated_query))
            predictive_alignment = extended_key_lengths * sigmoid_query

            ai_start = predictive_alignment - self._attention_window_size
            ai_end = predictive_alignment + self._attention_window_size

            std = torch.FloatTensor([self._attention_window_size / 2.]).pow(2)
            alignment_point_dist = (
                extended_key_lengths - predictive_alignment).pow(2)

            alignment_point_dist = (-(alignment_point_dist /
                                      (2 * std[0]))).exp()
            weight = weight * alignment_point_dist

            contexts = []
            for i in range(weight.size(0)):
                start = ai_start[i].int().data.numpy()[0]
                end = ai_end[i].int().data.numpy()[0]

                aligned_weight = weight[i, start:end]
                aligned_keys = keys[i, start:end]

                aligned_context = aligned_weight.unsqueeze(1) * aligned_keys
                contexts.append(aligned_context.sum(0))

            total_context = torch.stack(contexts, 0)
        elif self._alignment == "global":
            context = weight.unsqueeze(2) * keys
            total_context = context.sum(1)

        return total_context, alignment_score

    @property
    def attention_window_size(self):
        return self._attention_window_size


class MultiHeadAttention(nn.Module):
    def __init__(self,
                 query_dim,
                 key_dim,
                 num_units,
                 dropout_p=0.5,
                 h=8,
                 is_masked=False):
        super(MultiHeadAttention, self).__init__()

        if query_dim != key_dim:
            raise ValueError("query_dim and key_dim must be the same")
        if num_units % h != 0:
            raise ValueError("num_units must be dividable by h")
        if query_dim != num_units:
            raise ValueError("to employ residual connection, the number of "
                             "query_dim and num_units must be the same")

        self._dropout_p = dropout_p
        self._num_units = num_units
        self._h = h
        self._key_dim = Variable(torch.FloatTensor([key_dim]))
        self._key_dim = self._key_dim.cuda() if torch.cuda.is_available() else self._key_dim
        self._is_masked = is_masked

        self.query_layer = nn.Linear(query_dim, num_units, bias=False)
        self.key_layer = nn.Linear(key_dim, num_units, bias=False)
        self.value_layer = nn.Linear(key_dim, num_units, bias=False)
        self.bn = nn.BatchNorm1d(num_units)

    def forward(self, query, keys, residual=True, training=False):
        Q = self.query_layer(query)
        K = self.key_layer(keys)
        V = self.value_layer(keys)

        # split each Q, K and V into h different values from dim 2
        # and then merge them back together in dim 0
        chunk_size = int(self._num_units / self._h)
        Q = torch.cat(Q.split(split_size=chunk_size, dim=2), dim=0)
        K = torch.cat(K.split(split_size=chunk_size, dim=2), dim=0)
        V = torch.cat(V.split(split_size=chunk_size, dim=2), dim=0)

        # calculate QK^T
        attention = torch.matmul(Q, K.transpose(1, 2))
        # normalize with sqrt(dk)
        attention = attention / torch.sqrt(self._key_dim)
        # use masking (usually for decoder) to prevent leftward
        # information flow and retains auto-regressive property
        # as said in the paper
        if self._is_masked:
            diag_vals = attention[0].sign().abs()
            diag_mat = diag_vals.tril()
            diag_mat = diag_mat.unsqueeze(0).expand(attention.size())

            mask = Variable(
                torch.ones(diag_mat.size()) * (-2**32 + 1), requires_grad=False)
            mask = mask.cuda() if torch.cuda.is_available() else mask

            attention = (attention * diag_mat) + (mask * (diag_mat-1).abs())
        # put it to softmax
        attention = F.softmax(attention, dim=-1)
        # apply dropout
        if training:
            attention = F.dropout(attention, self._dropout_p)
        # multiplyt it with V
        attention = torch.matmul(attention, V)
        # convert attention back to its input original size
        restore_chunk_size = int(attention.size(0) / self._h)
        attention = torch.cat(
            attention.split(split_size=restore_chunk_size, dim=0), dim=2)
        
        if residual:
            attention += query
        # apply batch normalization
        if training:
            attention = self.bn(attention.transpose(1, 2).contiguous()).transpose(1, 2)

        return attention

class FullyAttention(nn.Module):
    def __init__(self, dim):
        super(FullyAttention, self).__init__()
        self.fc = nn.Linear(dim, 1)

    def forward(self, x):
        x_flat = x.view(-1, x.size(-1))
        probs = self.fc(x_flat).view(x.size(0), x.size(1))
        weights = F.softmax(probs)
        out = weights.unsqueeze(1).bmm(x)
        return out.squeeze(1)

class MatchAttention(nn.Module):
    '''
    Input:  c: (batch_size, c_len, embed_dim)
            q: (batch_size, q_len, embed_dim)
    Output: (batch_size, c_len, embed_dim)
    '''
    def __init__(self, dim):
        super(MatchAttention, self).__init__()
        self.linear = nn.Linear(dim, dim)
    
    def forward(self, c, q, is_linear=True):
        batch = c.size(0)
        word_dim = c.size(2)
        q_len = q.size(1)
        
        if is_linear:
            c_feature = F.relu(self.linear(c))
            q_feature = F.relu(self.linear(q))
        else:
            c_feature = c
            q_feature = q
        # (batch, c_len, dim) * (batch, q_len, dim) -> (batch, c_len, q_len)
        attn = torch.bmm(c_feature, q_feature.transpose(1, 2))
        attn = F.softmax(attn.view(-1, q_len), -1).view(batch, -1, q_len)
        # (batch, c_len, q_len) * (batch, q_len, dim) -> (batch, c_len, dim)
        mix = torch.bmm(attn, q)
        return mix

class DotAttention(nn.Module):

    def __init__(self, dim):
        super(DotAttention, self).__init__()

        self.dim = dim
        self.attn_vector = nn.Parameter(
            torch.Tensor(1, dim), requires_grad=True)

    def get_mask(self):
        pass

    def forward(self, inputs, lengths=None):
        batch_size, max_len = inputs.size()[:2]
        
        # apply attention layer
        weights = torch.bmm(inputs,
                            self.attn_vector  # (1, dim)
                            .unsqueeze(0)  # (1, 1, dim)
                            .transpose(2, 1) # (1, dim, 1)
                            .repeat(batch_size, 1, 1)) # (batch_size, dim, 1))
        
        #print(weights.size())
        attn_energies = F.softmax(F.tanh(weights.squeeze(-1)))

        # create mask based on the sentence lengths
        #idxes = torch.arange(0, max_len, out=torch.LongTensor(max_len)).unsqueeze(0).cuda()  # some day, you'll be able to directly do this on cuda
        #mask = Variable((idxes < lengths.data.unsqueeze(1)).float())

        # apply mask and renormalize attention scores (weights)
        #masked = attn_weights * mask
        _sums = attn_energies.sum(-1).unsqueeze(1).expand_as(attn_energies)  # sums per row
        attn_weights = attn_energies / _sums

        # apply attention weights
        weighted = torch.mul(inputs, attn_weights.unsqueeze(-1).expand_as(inputs))

        # get the final fixed vector representations of the sentences
        representations = weighted.sum(1).squeeze(-1)

        return representations, attn_weights

class MatrixAttention(nn.Module):
    '''
    Input:
        - x1: ``(batch_size, c_len, embedding_dim)``
        - x2: ``(batch_size, q_len, embedding_dim)``
    Output:
        - ``(batch_size, c_len, q_len)``
    '''
    def __init__(self, scale_output=False):
        super(MatrixAttention, self).__init__()
        self._scale_output = scale_output

    def forward(self, x1, x2):
        tiled_x1 = x1.unsqueeze(2).expand(x1.size()[0],
                                          x1.size()[1],
                                          x2.size()[1],
                                          x1.size()[2])
        tiled_x2 = x2.unsqueeze(1).expand(x2.size()[0],
                                          x1.size()[1],
                                          x2.size()[1],
                                          x2.size()[2])
        result = (tiled_x1 * tiled_x2).sum(dim=-1)
        if self._scale_output:
            result *= math.sqrt(tiled_x1.size(-1))
        return result
