# !/usr/bin/env python
# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F

class Highway(nn.Module):

    def __init__(self,
                 in_size,
                 n_layers = 1,
                 act=F.relu):
        super(Highway, self).__init__()
        self._in_size = in_size
        self._layers = torch.nn.ModuleList([torch.nn.Linear(in_size, in_size * 2)
                                            for _ in range(n_layers)])
        self._act = act
        for layer in self._layers:
            layer.bias[in_size:].data.fill_(1)

    def forward(self, inputs):
        current_input = inputs
        for layer in self._layers:
            projected_input = layer(current_input)
            linear_part = current_input
            
            nonlinear_part = projected_input[:, (0 * self._in_size):(1 * self._in_size)]
            gate = projected_input[:, (1 * self._in_size):(2 * self._in_size)]
            nonlinear_part = self._act(nonlinear_part)
            gate = F.sigmoid(gate)
            current_input = gate * linear_part + (1 - gate) * nonlinear_part
        return current_input
