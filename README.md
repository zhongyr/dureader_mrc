## environments
```
python 2.7
PyTorch 0.3
```
## how to run
```
python run --train
```

## how to predict
```
python run --predict
```

You can configure the hyperparameters in the file `const.py`
