# !/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
try:
    reload(sys)
    sys.setdefaultencoding('utf8')
except:
    pass

import const
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from layers import weighted_sum, masked_softmax, replace_masked_values, last_dim_softmax
from layers.rnn import StackRNN, BiRNN
from layers.distributed import TimeDistributed
from layers.attention import MatrixAttention, MultiHeadAttention
from layers.embedding import WordEmbedding, CNNEmbedding
from layers.highway import Highway

class PointerNet(nn.Module):
    def __init__(self, hidden_size, dropout_p=0.2):
        super(PointerNet, self).__init__()
        self.dim = 2*hidden_size

        self.matrix_attention = MatrixAttention()

        self.modeling_layer = MultiHeadAttention(4*self.dim, 4*self.dim, 4*self.dim, is_masked=True, dropout_p=dropout_p)
        self.end_modeling_layer = MultiHeadAttention(8*self.dim, 8*self.dim, 8*self.dim, is_masked=True, dropout_p=dropout_p)

        self.start_fc = TimeDistributed(nn.Linear(8*self.dim, 1))
        self.end_fc = TimeDistributed(nn.Linear(12*self.dim, 1))

        self.dropout = nn.Dropout(p=dropout_p)

    def forward(self, q, q_mask, p, p_mask, batch_size, training=False):
        # N - batch_size
        # T - passages length
        # J - questions length
        # questions.size() - N x J x d
        # passages.size() - N x T x d
        # G: query aware representation of each passage word

        N = p.size(0)
        T = p.size(1)
        J = q.size(1)

        h0_shape = (2, N, 2*self.dim)

        S_p_q = self.matrix_attention(p, q)
        W_p_q = last_dim_softmax(S_p_q, q_mask)
        p_q = weighted_sum(q, W_p_q)    # shape (N, T)

        S_p_q_mask = replace_masked_values(S_p_q, q_mask.unsqueeze(1))
        S_q_p = S_p_q_mask.max(dim=-1)[0].squeeze(-1)  # shape: (N, T)
        W_q_p = masked_softmax(S_q_p, p_mask)    # shape: (N, T)
        q_p = weighted_sum(p, W_q_p)    # shape: (N, self.dim)
        tiled_q_p = q_p.unsqueeze(1).expand(N, T, self.dim)

        G = torch.cat([p, p_q, p * p_q, p * tiled_q_p], dim=-1)
        M = self.modeling_layer(G, G, training)

        G_M = torch.cat([G, M], dim=-1)
        if training:
            G_M = self.dropout(G_M)
        start = G_M.view(batch_size, -1, 8*self.dim)
        start_predict = self.start_fc(start).squeeze(-1)

        G_M = torch.cat([G, M], dim=-1)
        M_ = self.end_modeling_layer(G_M, G_M, training)

        G_M_ = torch.cat([G, M_], dim=-1)
        if training:
            G_M = self.dropout(G_M_)
        end = G_M_.view(batch_size, -1, 12*self.dim)
        end_predict = self.end_fc(end).squeeze(-1)

        #print(start_predict.size(), end_predict.size())
        return start_predict, end_predict


class ReaderNet(nn.Module):
    def __init__(self, vocab):
        super(ReaderNet, self).__init__()

        self.input_size = vocab.size
        self.embed_size = vocab.embed_dim
        self.hidden_size = const.hidden_size

        self.word_embedding = WordEmbedding(self.input_size, self.embed_size)

        self.q_highway = TimeDistributed(Highway(self.embed_size, n_layers=1, act=F.relu))
        self.q_reader = BiRNN(self.embed_size, self.hidden_size, dropout_p=const.dropout_p, 
                              num_layers=const.num_layers)

        # question match to passage
        self.p_highway = TimeDistributed(Highway(self.embed_size, n_layers=1, act=F.relu))
        self.p_reader = BiRNN(self.embed_size, self.hidden_size, dropout_p=const.dropout_p, 
                              num_layers=const.num_layers)

        self.pointer_net = PointerNet(self.hidden_size, dropout_p=const.dropout_p)

        self.dropout = nn.Dropout(p=const.dropout_p)
        self.criterion = nn.CrossEntropyLoss()

    def forward(self, passages_all, questions_all, answers, training=False):

        passages, p_mask = passages_all
        questions, q_mask = questions_all

        #print(passages.view(answers[0].size(0), -1).size())
        q_emb = self.q_highway(self.word_embedding(questions))
        p_emb = self.p_highway(self.word_embedding(passages))

        q_u = self.q_reader(q_emb)
        p_u = self.p_reader(p_emb)
    
        if training:
            q_u = self.dropout(q_u)
            p_u = self.dropout(p_u)

        start, end = self.pointer_net(q_u, q_mask, 
                                      p_u, p_mask, 
                                      answers[0].size(0), training)

        return start, end

    def get_loss(self, passages_all, questions_all, answers, training=True):
        start, end = self.forward(passages_all, questions_all, answers, training)

        start_loss = self.criterion(start, answers[0])
        end_loss = self.criterion(end, answers[1])

        loss = torch.mean(start_loss + end_loss)
        return loss    

    def evaluate(self, batch, passages_all, questions_all, answers, training=False, save_full_info=False):

        start, end = self.forward(passages_all, questions_all, answers, training)
        start_probs = F.softmax(start, -1)
        end_probs = F.softmax(end, -1)
        # change to cpu
        start_probs = start_probs.data.cpu()
        end_probs = end_probs.data.cpu()

        padded_p_len = len(batch['passage_token_ids'][0])
        pred_answers, ref_answers = [], []

        for sample, start_prob, end_prob in zip(batch['raw_data'], start_probs, end_probs):
            best_answer = self.find_best_answer(sample, start_prob, end_prob, padded_p_len)
            answers = [best_answer]
            #answers = self.get_answers(sample, start_prob, end_prob, padded_p_len)
            if save_full_info:
                sample['pred_answers'] = [best_answer]
                pred_answers.append(sample)
            else:
                pred_answers.append({'question_id': sample['question_id'],
                                     #'question_type': sample['question_type'],
                                     'question_type': 'MAIN',
                                     'answers': answers,
                                     'entity_answers': [[]],
                                     'yesno_answers': []})
            if 'answers' in sample:
                ref_answers.append({'question_id': sample['question_id'],
                                     'question_type': sample['question_type'],
                                     'answers': sample['answers'],
                                     'entity_answers': [[]],
                                     'yesno_answers': []})
        return pred_answers, ref_answers

    def find_best_answer(self, sample, start_prob, end_prob, padded_p_len):
        """
        Finds the best answer for a sample given start_prob and end_prob for each position.
        This will call find_best_answer_for_passage because there are multiple passages in a sample
        """
        best_p_idx, best_span, best_score = None, None, 0
        for p_idx, passage in enumerate(sample['passages']):
            if p_idx >= const.max_p_num:
                continue
            passage_len = min(const.max_p_len, len(passage['passage_tokens']))
            answer_span, score = self.find_best_answer_for_passage(
                start_prob[p_idx * padded_p_len: (p_idx + 1) * padded_p_len],
                end_prob[p_idx * padded_p_len: (p_idx + 1) * padded_p_len],
                passage_len)
            if score > best_score:
                best_score = score
                best_p_idx = p_idx
                best_span = answer_span
        if best_p_idx is None or best_span is None:
            best_answer = ''
        else:
            best_answer = ''.join(
                sample['passages'][best_p_idx]['passage_tokens'][best_span[0]: best_span[1] + 1])
        return best_answer

    def find_best_answer_for_passage(self, start_probs, end_probs, passage_len=None):
        """
        Finds the best answer with the maximum start_prob * end_prob from a single passage
        """
        if passage_len is None:
            passage_len = len(start_probs)
        else:
            passage_len = min(len(start_probs), passage_len)
        best_start, best_end, max_prob = -1, -1, 0
        for start_idx in range(passage_len):
            for ans_len in range(const.max_a_len):
                end_idx = start_idx + ans_len
                if end_idx >= passage_len:
                    continue
                prob = start_probs[start_idx] * end_probs[end_idx]
                if prob > max_prob:
                    best_start = start_idx
                    best_end = end_idx
                    max_prob = prob
        return (best_start, best_end), max_prob
