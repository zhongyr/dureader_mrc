# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

train_fname = 'dureader/trainset/search.train.json'
train_tname = 'min_data/trainset/search.train.json'
dev_fname = 'dureader/devset/search.dev.json'
dev_tname = 'min_data/devset/search.dev.json'
test_fname = 'dureader/testset/search.test.json'
test_tname = 'min_data/testset/search.test.json'

def get_file(src, target, num):
    qtype = 'DESCRIPTION'
    with open(src, 'r') as f, open(target, 'w') as wf:
        i = 0
        for line in f:
            if i > num:
                break
            line = line.strip()
            data = json.loads(line)
            if data['question_type'] == qtype:
                wf.writelines(line + '\n')
                i += 1

get_file(train_fname, train_tname, 2000)
get_file(test_fname, test_tname, 800)
get_file(dev_fname, dev_tname, 500)



