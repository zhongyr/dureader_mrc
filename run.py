# -*- coding:utf8 -*-

import warnings
warnings.filterwarnings("ignore")

import sys
try:
    reload(sys)
    sys.setdefaultencoding('utf8')
except:
    pass

import numpy as np
import const
import os
import pickle
import json
import argparse
import logging
from dataset import BRCDataset
from vocab import Vocab
from utils import save_model, load_model, compute_bleu_rouge, normalize

from model import ReaderNet
from layers import get_mask

import torch
import torch.nn as nn
import torch.nn.utils as utils
import torch.optim as optim
from torch.autograd import Variable

torch.manual_seed(111)

def parse_args():
    """
    Parses command line arguments.
    """
    parser = argparse.ArgumentParser('Reading Comprehension on BaiduRC dataset')
    parser.add_argument('--prepare', action='store_true',
                        help='create the directories, prepare the vocabulary and embeddings')
    parser.add_argument('--train', action='store_true',
                        help='train the model')
    parser.add_argument('--no_eval', action='store_true' ,
                        help='not evaluate the model on dev set')
    parser.add_argument('--predict', action='store_true',
                        help='predict the answers for test set with trained model')
    parser.add_argument('--log', action='store_true',
                        help='predict the answers for test set with trained model')
    return parser.parse_args()


def prepare(args):
    """
    checks data, creates the directories, prepare the vocabulary and embeddings
    """
    logger = logging.getLogger("brc")
    logger.info('Checking the data files...')
    for data_path in const.train_path + const.dev_path + const.test_path:
        assert os.path.exists(data_path), '{} file does not exist.'.format(data_path)
    logger.info('Preparing the directories...')
    for dir_path in [const.vocab_dir, const.model_dir, const.result_dir]:
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    logger.info('Building vocabulary...')
    dataset = BRCDataset(const.max_p_num, const.max_p_len, const.max_q_len,
                          const.train_path, const.dev_path, const.test_path)
    vocab = Vocab(lower=True)
    for word in dataset.word_iter('train'):
        vocab.add(word)

    unfiltered_vocab_size = vocab.size
    vocab.filter_tokens_by_cnt(min_cnt=2)
    filtered_num = unfiltered_vocab_size - vocab.size
    logger.info('After filter {} tokens, the final vocab size is {}'.format(filtered_num, vocab.size))

    logger.info('Assigning embeddings...')
    vocab.randomly_init_embeddings(const.embed_size)

    logger.info('Saving vocab...')
    with open(os.path.join(const.vocab_dir, 'vocab.data'), 'wb') as fout:
        pickle.dump(vocab, fout)

    logger.info('Done with preparing!')


def train(args):
    """
    trains the reading comprehension model
    """
    logger = logging.getLogger("brc")
    logger.info('Load data_set and vocab...')
    with open(os.path.join(const.vocab_dir, 'vocab.data'), 'rb') as fin:
        vocab = pickle.load(fin)
    dataset = BRCDataset(const.max_p_num, const.max_p_len, const.max_q_len, const.train_path, const.dev_path)
    # idx...
    dataset.convert_to_ids(vocab)

    best_score = 0.0
    model = ReaderNet(vocab)
    model, start_epoch, best_score = load_model(model)
    model = model.cuda() if const.use_cuda else model
    #optimizer = optim.SGD(model.parameters(), lr=const.lr_rate, momentum=0.9, weight_decay=0)
    optimizer = torch.optim.Adam(model.parameters(), lr=const.lr_rate)


    for epoch in range(1, const.epochs + 1):

        model.train(True)

        for batch_idx, batch in enumerate(dataset.next_batch('train', const.batch_size, vocab.get_id(vocab.pad_token), shuffle=True)):
            # passage
            passages = torch.LongTensor(batch['passage_token_ids'])
            passages_mask = get_mask(passages, torch.LongTensor(batch['passage_length']))
            passages = passages.cuda() if const.use_cuda else  passages
            passages = Variable(passages)
            # questions
            questions = torch.LongTensor(batch['question_token_ids'])
            questions_mask = get_mask(questions, torch.LongTensor(batch['question_length']))
            questions = questions.cuda() if const.use_cuda else  questions
            questions = Variable(questions)

            answer_starts = torch.LongTensor(batch['start_id'])
            answer_starts = answer_starts.cuda() if const.use_cuda else  answer_starts
            answer_starts = Variable(answer_starts)

            answer_ends = torch.LongTensor(batch['end_id'])
            answer_ends = answer_ends.cuda() if const.use_cuda else  answer_ends
            answer_ends = Variable(answer_ends)

            loss = model.get_loss((passages, passages_mask), (questions, questions_mask), (answer_starts, answer_ends))
            if batch_idx % const.print_every == 0:
                print('epoch %d , batch_idx: %d,  loss: %.4f' % (epoch, batch_idx, loss.data[0]))

            optimizer.zero_grad()
            loss.backward()
            utils.clip_grad_norm(model.parameters(), const.grad_clip)
            optimizer.step()

        
        if not args.no_eval:
            bleu_rouge = evaluate(model, dataset, vocab)
            scores = []
            for k,v in bleu_rouge.items():
                if 'Bleu-4' == k or 'Rouge-L' == k:
                    scores.append(v)
            avg_score = np.mean(scores)
            print("avg score:{} \n bleu_rouge: {}".format(avg_score, bleu_rouge))
            if avg_score > best_score:
                print('save model')
                best_score = avg_score
                save_model(model, epoch, avg_score)

    logger.info('Done with model training!')

def evaluate(model, dataset, vocab):
    model.eval()

    pred_answers, ref_answers = [], []

    for batch_idx, batch in enumerate(dataset.next_batch('dev', const.batch_size, vocab.get_id(vocab.pad_token), shuffle=False)):
        # passage
        passages = torch.LongTensor(batch['passage_token_ids'])
        passages_mask = get_mask(passages, torch.LongTensor(batch['passage_length']))
        passages = passages.cuda() if const.use_cuda else  passages
        passages = Variable(passages)

        # questions
        questions = torch.LongTensor(batch['question_token_ids'])
        questions_mask = get_mask(questions, torch.LongTensor(batch['question_length']))
        questions = questions.cuda() if const.use_cuda else  questions
        questions = Variable(questions)
    
        answer_starts = torch.LongTensor(batch['start_id'])
        answer_starts = answer_starts.cuda() if const.use_cuda else  answer_starts
        answer_starts = Variable(answer_starts)

        answer_ends = torch.LongTensor(batch['end_id'])
        answer_ends = answer_ends.cuda() if const.use_cuda else  answer_ends
        answer_ends = Variable(answer_ends)      
        
        pred_answer, ref_answer = model.evaluate(batch, (passages, passages_mask), (questions, questions_mask), (answer_starts, answer_ends))
        pred_answers.extend(pred_answer)
        ref_answers.extend(ref_answer)

    # compute the bleu and rouge scores if reference answers is provided
    if len(ref_answers) > 0:
        pred_dict, ref_dict = {}, {}
        for pred, ref in zip(pred_answers, ref_answers):
            question_id = ref['question_id']
            if len(ref['answers']) > 0:
                pred_dict[question_id] = normalize(pred['answers'])
                ref_dict[question_id] = normalize(ref['answers'])
        bleu_rouge = compute_bleu_rouge(pred_dict, ref_dict)
    else:
        bleu_rouge = 0

    model.train()

    return bleu_rouge

def predict(args):
    """
    predicts answers for test files
    """
    logger = logging.getLogger("brc")
    logger.info('Load data_set and vocab...')
    with open(os.path.join(const.vocab_dir, 'vocab.data'), 'rb') as fin:
        vocab = pickle.load(fin)
    assert len(const.test_path) > 0, 'No test files are provided.'

    # output file
    result_file = os.path.join(const.result_dir, 'test.predicted.json') 
    try:
        os.remove(result_file)
    except:
        pass

    dataset = BRCDataset(const.max_p_num, const.max_p_len, const.max_q_len, test_files=const.dev_path)
    logger.info('Converting text into ids...')
    dataset.convert_to_ids(vocab)
    logger.info('Restoring the model...')
    model = ReaderNet(vocab)
    model, start_epoch, best_score = load_model(model)
    model = model.cuda() if const.use_cuda else model

    model.eval()

    logger.info('Predicting answers for test set...')

    for batch_idx, batch in enumerate(dataset.next_batch('test', const.batch_size, vocab.get_id(vocab.pad_token), shuffle=False)):
        # passage
        passages = torch.LongTensor(batch['passage_token_ids'])
        passages_mask = get_mask(passages, torch.LongTensor(batch['passage_length']))
        passages = passages.cuda() if const.use_cuda else  passages
        passages = Variable(passages)

        # questions
        questions = torch.LongTensor(batch['question_token_ids'])
        questions_mask = get_mask(questions, torch.LongTensor(batch['question_length']))
        questions = questions.cuda() if const.use_cuda else  questions
        questions = Variable(questions)

        answer_starts = torch.LongTensor(batch['start_id'])
        answer_starts = answer_starts.cuda() if const.use_cuda else  answer_starts
        answer_starts = Variable(answer_starts)

        answer_ends = torch.LongTensor(batch['end_id'])
        answer_ends = answer_ends.cuda() if const.use_cuda else  answer_ends
        answer_ends = Variable(answer_ends)      
        
        pred_answer, ref_answer = model.evaluate(batch, (passages, passages_mask), (questions, questions_mask), (answer_starts, answer_ends))

        with open(result_file, 'a') as fout:
            for answer in pred_answer:
                fout.write(json.dumps(answer, encoding='utf8', ensure_ascii=False) + '\n')


def run():
    """
    Prepares and runs the whole system.
    """
    args = parse_args()

    logger = logging.getLogger("brc")
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    if args.log:
        file_handler = logging.FileHandler(const.log_path)
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    else:
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)

    logger.info('Running with args : {}'.format(args))

    if args.prepare:
        prepare(args)
    if args.train:
        train(args)
    if args.predict:
        predict(args)

if __name__ == '__main__':
    run()
